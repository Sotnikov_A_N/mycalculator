package Expression;
import java.util.Map;
/**
 * Created by Антон on 08.11.2014.
 */
public abstract class Expression {
    public abstract Object execute(Map<String, Object> values) throws Exception;

    /*узел  - число*/
    static class Num extends Expression {
        private final long value;

        public Num(long x) {
            value = x;
        }
        @Override
        public Object execute(Map<String, Object> values) {
            return value;
        }
    }

    /*узел - бинарный оператор*/
    static class Binary extends Expression {
        private final Expression x1;
        private final Expression x2;
        private final String op;

        public Binary(Expression x1, Expression x2, String op) {
            this.x1 = x1;
            this.x2 = x2;
            this.op = op;
        }

        @Override
        public Object execute(Map<String, Object> values) throws Exception {
            Object o1 = x1.execute(values);
            Object o2 = x2.execute(values);

            Class type = commonType(o1, o2);
                return execNum((Long)o1, (Long)o2);
        }

        private Class commonType(Object o1, Object o2) {
                return Long.class;
        }

        private Object execNum(long n1, long n2) throws Exception {
            if("+".equals(op))
                return (Long)(n1 + n2);
            if("-".equals(op))
                return (Long)(n1 - n2);
            if("*".equals(op))
                return (Long)(n1 * n2);
            if("/".equals(op))
                return (Long)(n1 / n2);

            throw new Exception("Illegal Long operator: " + op);
        }
    }

}

