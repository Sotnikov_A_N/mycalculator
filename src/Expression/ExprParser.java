package Expression;

/**
 * Created by Антон on 08.11.2014.
 */
import java.util.HashMap;
import java.util.Map;



/** Компилятор выражений */
public class ExprParser {

    private String expression; // Строка с исходным выражением
    private int p = 0; // текущая позиция

    public static Expression build(String expression) throws Exception {
        ExprParser builder = new ExprParser(expression);
        builder.skip(" ");
        Expression expr = builder.build(0);
        return expr;
    }

    private ExprParser(String expression) {
        this.expression = expression;
    }


    /** Построить узел выражения */
    Expression build(int state) throws Exception {
        if(lastState(state)) {
            Expression ex = null;
            if(startWith("(")) {
                skip("(");
                ex = build(0);
                skip(")");
            }
            else
                ex = readSingle();
            return ex;
        }

    /* Строим первый операнд */
        Expression a1 = build(state+1);
        String op = null;
        while((op = readStateOperator(state)) != null) {
            Expression a2 = build(state + 1);
            a1 = new Expression.Binary(a1, a2, op);

        }
        return a1;
    }

    private static String [][] states = new String[][] {
            {"+", "-"},
            {"*", "/"},
            null
    };

    private boolean lastState(int s) {
        return s+1 >= states.length;
    }

    private boolean startWith(String s) {
        return expression.startsWith(s, p);
    }

    private void skip(String s) {
        if(startWith(s))
            p+= s.length();
        while(p < expression.length() && expression.charAt(p) == ' ')
            p++;
    }


    private String readStateOperator(int state) {
        String[] ops = states[state];
        for(String s : ops) {
            if(startWith(s)) {
                skip(s);
                return s;
            }
        }
        return null;
    }

    /**
     * считываем из потока "простое" значение
     * @return
     */
    private Expression readSingle() throws Exception {
        int p0 = p;
        while(p < expression.length()) {
            if((!(Character.isLetterOrDigit(expression.charAt(p))))){
            break;
            }
            p++;
        }

        Expression ex = null;
        if(p > p0) {
            String s = expression.substring(p0, p);
            skip(" ");
            try{
                // из потока прочитали число
                long x = Long.parseLong(s);
                return new Expression.Num(x);
            }
            catch(Exception e){}
        }
        return null;
    }
}