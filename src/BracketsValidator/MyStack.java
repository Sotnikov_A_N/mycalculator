package BracketsValidator;

/**
 * Created by sotnokov_a_n on 06.11.14.
 */
public class MyStack {
    static int size;
    static Object[] elements = new Object[5];

    static void push(Object element) {
        assert element != null;
        ensureCapacity(size + 1);
        elements[size++] = element;
    }

    static void ensureCapacity(int capacity) {
        if (capacity <= elements.length) {
            return;
        }
        Object[] newElements = new Object[2 * capacity];
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }
        elements = newElements;

    }

    static Object pop() {
        assert size > 0;
        return elements[--size];
    }

    static Object peek() {
        assert size > 0;
        return elements[size - 1];
    }

    static int size() {
        return size;
    }

    static boolean isEmpty() {
        return size == 0;
    }
}
