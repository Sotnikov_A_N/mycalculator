package BracketsValidator;

/**
 * Created by sotnokov_a_n on 06.11.14.
 */
public class BracketsValidator {
    private MyStack st = new MyStack();

    private boolean isOpeningBracket(char bracket) {
        return "({[".indexOf(bracket) != -1;
    }

    private boolean isClosingBracket(char bracket) {
        return ")}]".indexOf(bracket) != -1;
    }

    private boolean isPair(char opening, char closing) {
        return opening == '(' && closing == ')' || opening == '['
                && closing == ']' || opening == '{' && closing == '}';
    }

    public boolean validate(String input) {
        for (char c : input.toCharArray()) {
            if (isClosingBracket(c) && st.isEmpty()) {
                return false;
            }
            if (isOpeningBracket(c)) {
                st.push(c);
            }
            if (isClosingBracket(c)) {
                if (isPair(st.peek().toString().charAt(0), c)) {
                    st.pop();
                } else {
                    return false;
                }
            }
        }
        return st.isEmpty();
    }

}
