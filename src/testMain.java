import Expression.ExprParser;
import Expression.Expression;
import BracketsValidator.BracketsValidator;

import java.io.IOException;
import java.util.*;

/**
 * Created by Антон on 08.11.2014.
 */
public class testMain {
    public static void main(String[] args) throws Exception {
        Map<String, Object> mapper = new HashMap<String, Object>();
        BracketsValidator b1 = new BracketsValidator();
        String s = "((2+2)*2)+(2*2+2)";

        if  (b1.validate(s)) {
            Expression e = ExprParser.build(s);
            Long a = (Long) e.execute(mapper);
            System.out.print(a);
        }
    }
}
